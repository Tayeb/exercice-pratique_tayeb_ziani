package fr.carbonIT.builder;

import java.util.List;

import fr.carbonIT.model.aventurier.Aventurier;
import fr.carbonIT.model.carte.Carte;
import fr.carbonIT.model.position.Orientation;


public interface ICarteBuildeur {
	
	public void creerCarte(int nombreLignes, int nombreColones);
	
	public void creerCase(int positionHorizontale,int positionVertical);
	
	public void placerTresor(int positionHorizontal, int positionVertical, int nombreTresor);
	
	public void creerAventurier(String nom, int positionHorizontal, int positionVertical, Orientation orientation, String sequence);
	
	public Carte recupererCarte();
	
	public List<Aventurier> recupererAventeriers();
	
}
