package fr.carbonIT.visiteur;

import java.util.ArrayList;
import java.util.List;

import fr.carbonIT.model.aventurier.Aventurier;
import fr.carbonIT.model.carte.Carte;
import fr.carbonIT.model.cases.ICase;

public class ConcreteVisiteur implements IVisiteur{
	
	private Carte carte;
	
	public ConcreteVisiteur(Carte carte) {
		// TODO Auto-generated constructor stub
		this.carte = carte;
	}
	
	@Override
	public List<ICase> getMontagnes() {
		// TODO Auto-generated method stub
		ICase[][] cases = this.carte.getCases();
		List<ICase> casesMontagnes = new ArrayList<ICase>();
		for (int i = 0; i <= this.carte.getBorneHorizental(); i++) {
			for (int j = 0; j <= this.carte.getBorneVertical(); j++) {
				if (cases[i][j].getNature() == "M") {
					casesMontagnes.add(cases[i][j]);
				}
			}
		}
 		return casesMontagnes;
	}
	
	@Override
	public List<ICase> getTresors() {
		// TODO Auto-generated method stub
		ICase[][] cases = this.carte.getCases();
		List<ICase> casesAtresors = new ArrayList<ICase>();
		for (int i = 0; i <= this.carte.getBorneHorizental(); i++) {
			for (int j = 0; j <= this.carte.getBorneVertical(); j++) {
				if (cases[i][j].getNombreTresor() > 0) {
					casesAtresors.add(cases[i][j]);
				}
			}
		}
 		return casesAtresors;
	}

	@Override
	public List<Aventurier> getAventeriers() {
		// TODO Auto-generated method stub
		ICase[][] cases = this.carte.getCases();
		List<Aventurier> aventuriers = new ArrayList<Aventurier>();
		for (int i = 0; i <= this.carte.getBorneHorizental(); i++) {
			for (int j = 0; j <= this.carte.getBorneVertical(); j++) {
				if (cases[i][j].getAventurier() != null) {
					aventuriers.add(cases[i][j].getAventurier());
				}
			}
		}
 		return aventuriers;
	}

}
