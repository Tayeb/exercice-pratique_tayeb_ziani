package fr.carbonIT.visiteur;


import java.util.List;

import fr.carbonIT.model.aventurier.Aventurier;
import fr.carbonIT.model.cases.ICase;

public interface IVisiteur {
	
	public List<ICase> getMontagnes();
	
	public List<ICase> getTresors();
	
	public List<Aventurier> getAventeriers();
	
}
