package fr.carbonIT.model.cases;

import fr.carbonIT.model.aventurier.Aventurier;
import fr.carbonIT.model.position.Position;

/*
 * pour généraliser le traitement des cases par un seule Type "ICase",
 * Cette interface Contient des méthodes avec un mot clé "default" du java 8. 
 */
public interface ICase {
	
	/*
	 * Cette méthode est utilisée pour vérifier si la case est valide 
	 * pour qu'un aventurier avance sinon il reste à sa place.
	 * NB: "cette methode est implémenter par les deux sous type 'Plaine' et 'Montagne'".
	 */
	public boolean estValid();
	
	/*
	 * Cette méthode est implémentée seulement dans la classe "Plaine" ainsi que les méthodes qu'elles suivent
	 *  puisque seule la case 'Plaine' peut contenir des tresors.
	 */  
	default public boolean EstCeQueContientTresor() { return false;	}
	
	default public void decrementerNombreTresor() {}
	
	default public int getNombreTresor() { return 0; }
	
	default public void setNombreTresor(int nombreTresor) {}
	
	/*
	 * Cette methode est implementer seulement dans la classe "Plaine" ainsi que les méthodes qu'elles suivent 
	 * puisque seule la case 'Plaine' peut contenir un aventurier
	 */
	default public Aventurier getAventurier() { return null; }
	
	default public void setAventurier(Aventurier aventurier) { }
	
	// Cette méthode sera utilisé pour indiquer qu'une case à un aventurier dedans ou non.
	default public void setValide(boolean valide) {};
	
	public Position getPosition();
	
	public String getNature();
	
	
	
}
