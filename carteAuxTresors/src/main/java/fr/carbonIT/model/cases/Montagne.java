package fr.carbonIT.model.cases;

import fr.carbonIT.model.position.Position;

public class Montagne implements ICase {
	
	private static String NATURE = "M";
	
	private Position position;
	
	public Montagne(Position position) {
		// TODO Auto-generated constructor stub
		this.position = position;
	}
	
	@Override
	public boolean estValid() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public String getNature() {
		// TODO Auto-generated method stub
		return Montagne.NATURE;
	}
	
	@Override
	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}
	
}
