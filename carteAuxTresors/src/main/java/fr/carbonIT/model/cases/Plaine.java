package fr.carbonIT.model.cases;

import fr.carbonIT.model.aventurier.Aventurier;
import fr.carbonIT.model.position.Position;

public class Plaine implements ICase{
	
	private static final String NATURE = ".";
	
	private int nombreTresor;
	private boolean valide;
	
	private Position position;
	private Aventurier aventurier;
	
	public Plaine(Position position) {
		// TODO Auto-generated constructor stub
		this.position = position;
		this.valide = true;
	}
	
	@Override
	public boolean EstCeQueContientTresor() {
		return this.nombreTresor > 0 ? true : false;
	}
	
	public void decrementerNombreTresor() {
		this.nombreTresor--;
	}
	
	@Override
	public String getNature() {
		// TODO Auto-generated method stub
		return Plaine.NATURE;
	}
	
	@Override
	public boolean estValid() {
		// TODO Auto-generated method stub
		return valide;
	}
	
	@Override
	public void setValide(boolean valide) {
		this.valide = valide;
	}
	
	@Override
	public Position getPosition() {
		return position;
	}
	
	public void setPosition(Position position) {
		this.position = position;
	}

	public int getNombreTresor() {
		return nombreTresor;
	}

	public void setNombreTresor(int nombreTresor) {
		this.nombreTresor = nombreTresor;
	}

	public Aventurier getAventurier() {
		return aventurier;
	}

	public void setAventurier(Aventurier aventurier) {
		this.aventurier = aventurier;
	}	
	
}
