package fr.carbonIT.model.position;

public enum Orientation {
	
	Est,
	Sud,
	Ouest,
	Nord;
	
	// Pour changer l'orientation de l'aventurier vers la droite
	public Orientation droite() {
		switch (this) {
		case Est:
			return Sud;
		case Sud:
			return Ouest;
		case Ouest:
			return Nord;
		case Nord:
			return Est;
		}
		throw new AssertionError("Opération inconnue");
	}
	
	// Pour changer l'orientation de l'aventurier vers la guauche
	public Orientation guache() {
		switch (this) {
		case Est:
			return Nord;
		case Nord:
			return Ouest;
		case Ouest:
			return Sud;
		case Sud:
			return Est;

		}
		throw new AssertionError("Opération inconnue");
	}
}
