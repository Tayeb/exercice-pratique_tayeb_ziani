package fr.carbonIT.model.aventurier;


import fr.carbonIT.model.carte.Carte;
import fr.carbonIT.model.cases.ICase;
import fr.carbonIT.model.position.Orientation;
import fr.carbonIT.model.position.Position;
import fr.carbonIT.util.GestionnairePosition;

public class Aventurier {
	
	private static Carte carte;
	private static GestionnairePosition gestionnaire;
	
	private int indexSequence = 0;
	private int nombreTresorsTrouver = 0;
	
	private String nom;
	private String sequence;
	private Position position;
	private Orientation orientation;

	public Aventurier(String nom, Position position, Orientation oriention, String sequence, Carte carte) {
		// TODO Auto-generated constructor stub
		this.nom = nom;
		this.position = position;
		this.sequence = sequence;
		this.orientation = oriention;
		Aventurier.carte = carte;
	}

	public void avancer() {
		if (Aventurier.carte.verifierAvancement(this.position, this.orientation)) {
			int positionVertical = this.position.getVertical();
			int posisitonHorzontal = this.position.getHorizental();
			if (this.orientation == Orientation.Est) {
				positionVertical++;
			} else if (this.orientation == Orientation.Ouest) {
				positionVertical--;
			} else if (this.orientation == Orientation.Sud) {
				posisitonHorzontal++;
			} else if (this.orientation == Orientation.Nord) {
				posisitonHorzontal--;
			}
			this.setPosition(new Position(posisitonHorzontal, positionVertical));
			Aventurier.gestionnaire.gererPosition(this);
			this.chercherTresor();
		}
	}

	private void chercherTresor() {
		ICase c = Aventurier.carte.getCase(this.position);
		if (c.EstCeQueContientTresor()) {
			this.ramaserTresor(c);
		}
	}

	private void ramaserTresor(ICase c) {
		this.incrementerNombreTresorTrouver();
		c.decrementerNombreTresor();
	}
	
	public static void setGestionnaireInstance(GestionnairePosition gestionnaire) {
		Aventurier.gestionnaire = gestionnaire;
	}
	
	public void incrementerIndexSequence() {
		this.indexSequence = this.indexSequence + 1;
	}
	
	public int getIndexSequence() {
		return this.indexSequence;
	}
	
	public void incrementerNombreTresorTrouver() {
		this.nombreTresorsTrouver++;
	}
	
	public int getNombreTresorTrouver() {
		return this.nombreTresorsTrouver;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public Orientation getOrientation() {
		return orientation;
	}

	public void setOrientation(Orientation orientation) {
		this.orientation = orientation;
	}

	public static Carte getCarte() {
		return Aventurier.carte;
	}
	
}
