package fr.carbonIT.model.carte;

import fr.carbonIT.model.cases.ICase;
import fr.carbonIT.model.cases.Plaine;
import fr.carbonIT.model.position.Orientation;
import fr.carbonIT.model.position.Position;

public class Carte {
	
	private int borneHorizental;
	private int borneVertical;
	
	private ICase[][] iCases;
	
	public Carte(int nombreLignes, int nombreColones) {
		this.borneHorizental = nombreLignes - 1;
		this.borneVertical = nombreColones - 1;
		this.iCases = new ICase[nombreLignes][nombreColones];
		this.initialiserLaCarte();
	}
	
	//Cette methode et appelé par l'aventurier pour vérifier son prochain avancement 
	public boolean verifierAvancement(Position position, Orientation orientation){
		if(position.getHorizental() == 0 && orientation == Orientation.Nord) {
			return false;
		}else if(position.getHorizental() == borneHorizental && orientation == Orientation.Sud) {
			return false;
		}else if(position.getVertical() == 0 && orientation == Orientation.Ouest) {
			return false;
		}else if(position.getVertical() == borneVertical && orientation == Orientation.Est) {
			return false;
		}else if(orientation == Orientation.Nord) {
			return this.iCases[position.getHorizental() - 1][position.getVertical()].estValid();
		}else if(orientation == Orientation.Sud) {
			return this.iCases[position.getHorizental() + 1][position.getVertical()].estValid();
		}else if(orientation == Orientation.Est) {
			return this.iCases[position.getHorizental()][position.getVertical() + 1].estValid();
		}else {
			return this.iCases[position.getHorizental()][position.getVertical() - 1].estValid(); //Surement l'aventeurier va vers l'Ouest. 
		}
	}
	
	/*
	 * Pour inisialiser la carte en premier temps par des cases plaines
	 */
	private void initialiserLaCarte() {
		for (int i = 0; i <= this.borneHorizental; i++) {
			for (int j = 0; j <= this.borneVertical; j++) {
				Position position = new Position(i, j);
				ICase c = new Plaine(position);
				this.iCases[i][j] = c;
			}
		}
	}
	
	public int getBorneHorizental() {
		return this.borneHorizental;
	}

	public int getBorneVertical() {
		return this.borneVertical;
	}
	
	/*
	 * Pour permettre d'insérer une case "Montagne", puisque la carte est initialisée que par des cases "Plaine".
	 */
	public void modifierCase(ICase c) {
		this.iCases[c.getPosition().getHorizental()][c.getPosition().getVertical()] = c;
	}
	
	public ICase getCase(Position position) {
		return this.iCases[position.getHorizental()][position.getVertical()];
	}
	
	public ICase[][] getCases(){
		return this.iCases;
	}
	
}
