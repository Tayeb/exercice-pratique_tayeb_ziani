package fr.carbonIT.app;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import fr.carbonIT.builder.ConcreteCarteBuildeur;
import fr.carbonIT.builder.ICarteBuildeur;
import fr.carbonIT.directeur.Directeur;
import fr.carbonIT.model.aventurier.Aventurier;
import fr.carbonIT.model.carte.Carte;
import fr.carbonIT.model.cases.ICase;
import fr.carbonIT.visiteur.ConcreteVisiteur;
import fr.carbonIT.visiteur.IVisiteur;


public class App {
	
	private static Carte carte = null;
	private static List<Aventurier> aventuriers = null;
	private static BufferedWriter writer = null;
	
	
	public static void main(String[] args) throws IOException {
		
		InputStream src = App.class.getResourceAsStream("/MadreDiosCarte.entree");

		ICarteBuildeur builder = new ConcreteCarteBuildeur();

		Directeur directeur = new Directeur(builder, src);

		directeur.chargerLaCarteDepuisFichierPlat();

		App.carte = builder.recupererCarte();

		App.aventuriers = builder.recupererAventeriers();

		App.lancerLeJeux();

		IVisiteur visiteur = new ConcreteVisiteur(App.carte);

		App.aventuriers = visiteur.getAventeriers();

		List<ICase> tresors = visiteur.getTresors();

		List<ICase> montagnes = visiteur.getMontagnes();

		App.writer = new BufferedWriter(new FileWriter(new File("MadreDiosCarte.sortie")));

		App.serialiserLeResultatDansUnFichierPlat(montagnes, tresors, App.aventuriers);

		App.writer.close();
		
		System.out.println("Vous trouvez le resultat dans le fichier de sortie MadreDiosCarte.sortie dans le dossier racine.\n S\'il n\'est pas encore apparu, veuillez rafraîchir le projet !");
		
	}

	private static void lancerLeJeux() {
		int k = 1;
		int nombreCoup = App.getNombreCoup();
		while (k <= nombreCoup) {
			for (int i = 0; i < App.aventuriers.size(); i++) {
				if (App.aventuriers.get(i).getIndexSequence() < App.aventuriers.get(i).getSequence().length()) {
					if (App.aventuriers.get(i).getSequence().charAt(App.aventuriers.get(i).getIndexSequence()) == 'A') {
						App.aventuriers.get(i).avancer();
					}else if (App.aventuriers.get(i).getSequence().charAt(App.aventuriers.get(i).getIndexSequence()) == 'G') {
						App.aventuriers.get(i).setOrientation(App.aventuriers.get(i).getOrientation().guache());
					}else if (App.aventuriers.get(i).getSequence().charAt(App.aventuriers.get(i).getIndexSequence()) == 'D') {
						App.aventuriers.get(i).setOrientation(App.aventuriers.get(i).getOrientation().droite());
					}
				}
				App.aventuriers.get(i).incrementerIndexSequence();
			}
			k = k+1;
		}
	}

	private static int getNombreCoup() {
		int max = App.aventuriers.get(0).getSequence().length();
		for (int i = 1; i < App.aventuriers.size(); i++) {
			if (max > App.aventuriers.get(i).getSequence().length()) {
				max = App.aventuriers.get(i).getSequence().length();
			}
		}
		return max;
	}

	private static void serialiserLeResultatDansUnFichierPlat(List<ICase> montagnes, List<ICase> tresors, List<Aventurier> aventuriers) throws IOException {
		App.writer.write("C - " + (App.carte.getBorneVertical() + 1) + " - " + (App.carte.getBorneHorizental() + 1));

		App.writer.newLine();

		for (int i = 0; i < montagnes.size(); i++) {
			App.writer.write((montagnes.get(i).getNature() + " - " + montagnes.get(i).getPosition().getVertical() 
					+ " - " + montagnes.get(i).getPosition().getHorizental()));
			App.writer.newLine();
		}
		
		App.writer.write("# {T comme Trésor} - {Axe horizontal} - {Axe vertical} - {Nb. de trésors restants}");
		App.writer.newLine();

		for (int i = 0; i < tresors.size(); i++) {
			App.writer.write(("T - " + tresors.get(i).getPosition().getVertical() 
					+ " - " + tresors.get(i).getPosition().getHorizental() + " - " + tresors.get(i).getNombreTresor()));
			App.writer.newLine();
		}
		
		App.writer.write("# {A comme Aventurier} - {Nom de l’aventurier} - {Axe horizontal} - {Axe " + 
				"vertical} - {Orientation} - {Nb. trésors ramassés}");
		App.writer.newLine();
		
		for (int i = 0; i < aventuriers.size(); i++) {
			App.writer.write("A - " + aventuriers.get(i).getNom() + " - " 
					+ aventuriers.get(i).getPosition().getVertical() 
					+ " - " + aventuriers.get(i).getPosition().getHorizental() + " - " + aventuriers.get(i).getOrientation() 
					+ " - " + aventuriers.get(i).getNombreTresorTrouver());
			App.writer.newLine();
		}
	}
}
