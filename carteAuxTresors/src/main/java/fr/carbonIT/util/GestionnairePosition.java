package fr.carbonIT.util;

import fr.carbonIT.model.aventurier.Aventurier;
import fr.carbonIT.model.carte.Carte;
import fr.carbonIT.model.position.Position;

public class GestionnairePosition {

	private Carte carte;
	
	public GestionnairePosition(Carte carte) {
		// TODO Auto-generated constructor stub
		this.carte = carte;
	}

	public void gererPosition(Aventurier aventurier) {
		int positionVertical = aventurier.getPosition().getVertical();
		int positionHorizontal = aventurier.getPosition().getHorizental();
		switch (aventurier.getOrientation()) {
		case Nord:
			positionHorizontal++;
			break;
		case Est: 
			positionVertical--;
			break;
		case Sud:
			positionHorizontal--;
			break;
		case Ouest:
			positionVertical++;
			break;
		}
		/*
		 * pour désassocier l'aventurier avec l'ancienne case.
		 */
		Position anciennePosition = new Position(positionHorizontal, positionVertical);
		this.carte.getCase(anciennePosition).setAventurier(null);
		this.carte.getCase(anciennePosition).setValide(true); // pour indiquer que la case ne contient plus d'aventurier.
		/*
		 * pour associer l'aventurier avec la nouvelle case.
		 */
		this.carte.getCase(aventurier.getPosition()).setAventurier(aventurier);
		this.carte.getCase(aventurier.getPosition()).setValide(false); // pour indiquer que la case a un aventurier dedans. 
	}


}
